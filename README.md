# go-gin-mysql-test-dot

![screencapture-localhost-8000-api-docs-index-html-2022-04-16-21_42_51](https://i.ibb.co/T8XJDZL/13-1-flow.png)

![screencapture-localhost-8000-api-docs-index-html-2022-04-16-21_42_50](https://www.santekno.com/images/diagram-repository-pattern.png)

### Implement MVC and Repository Design Pattern
MVC Design pattern implemented in the code structure that has been created starting from controllers, models and repositories.

And for the repository design pattern itself, it is implemented in the repositories folder.

In the book Domain-Driven Design, Eric Evans explains that

A repository is a mechanism for encapsulating storage, retrieval, and search behavior, emulating a collection of objects. This Repository Pattern is usually used as a bridge between our application's business logic and all SQL commands into the Database. So we will write all the SQL in the repository while the business logic of our code only needs to use the repository that we have created.


### Golang Restful CRUD & Authentication API using Gin Framework, Gorm, and MySQL.

This Structure has been equipped with :
- GORM (Fantastic ORM Library)
- Validations
- Pagination
- JWT Authentication
- Documentation Swagger
- Migrations
- Handlers CORS
- Centralized Error Handling
- Logging Implementation
- Redis Implementation
- Example of Implementation Endpoints for All Methods (GET, POST, PUT, DELETE, PATCH) 
- Example Database Transaction (On Endpoint Bulk Create Books)

### Structure

```
.
├── config
│   └── development.yaml
│   └── production.yaml
│   └── stage.yaml
│   └── test.yaml
├── controllers
│   └── auth_controller.go
│   └── books_controller.go
│   └── default_controller.go
│   └── users_controller.go
├── docs
│   └── swagger.json
│   └── swagger.yaml
├── helper
│   ├── helper_responses.go
│   ├── paginations.go
├── lib
│   ├── config
│   │   ├── config.go
├── middlewares
│   ├── auth.go
├── migrations
│   ├── migrations.go
├── models
│   ├── books.go
│   ├── users.go
├── repositories
│   ├── auth_repository.go
│   ├── books_repository.go
│   ├── users_repository.go
├── responses
│   ├── responses.go
├── router
│   ├── router.go
│   ├── server.go
├── templates
│   ├── index.tmpl
├── test
├── utils
│   ├── token.go
├── validations
│   ├── auth_validation.go
│   ├── books_validation.go
├── .env
├── .gitignore
├── go.mod
├── go.sum
├── LICENSE
├── README.md
└── main.go

```

## Project Structure Details
**/controllers** :
This folder contains all controller classes and is added into the controller package.

**/models** :
This folder contains all interfaces, and structs and is added to the models package.

**/repositories** :
This folder contains interaction logic with the database starting from inserting data or deleting data.

**/route** :
This folder contains the route file and is added to the route package.

**/helpers** :
This folder contains the helper class, and constant class, and is added to the helper package.

**/docs** :
This folder contains the swagger documentation api 

**/lib/config** :
This folder contains the config in service as connection database or load data environment

**/migrations** :
This folder contains a list of migration tables using the gorm auto migrate.

**/utils** :
This folder contains functions to generate jwt tokens, check valid tokens and extract tokens into user data.

**/validations** :
This folder contains struct request with fill detail validation like min,max,one-of or required

**.env** :
This folder contains a configuration file, like we will add a .env file for the environment parameters of golang project.

**main.go** : 
file is the entry point for the application, where you’ll configure routes, start the server and initialize dependencies.

## Project Setup
### Set your .env in (env-example)
```
DB_HOST = localhost
DB_PORT = 8889
DB_USER = root
DB_PASSWORD = root
DB_NAME = db-test-dot
MODE = development

REDIS_HOST = localhost
REDIS_PORT = 6379

TOKEN_HOUR_LIFESPAN=24
API_SECRET=ZGFsYW0gc3VyYXQgYWwtTWFpZGFoIGF5YXQgMzg6IExha2ktbGFraSB5YW5nIG1lbmN1cmkgZGFuIHBlcmVtcHVhbiB5YW5nIG1lbmN1cmksIHBvdG9uZ2xhaCB0YW5nYW4ga2VkdWFueWEgKHNlYmFnYWkpIHBlbWJhbGFzYW4gYmFnaSBhcGEgeWFuZyBtZXJla2Ega2VyamFrYW4gZGFuIHNlYmFnYWkgc2lrc2FhbiBkYXJpIEFsbGFoLg==

; For Migration :
; -> give CREATE_MIGRATION_ALL = 0 to set off migration
; -> give CREATE_MIGRATION_ALL = 1 to migration all table
; -> give CREATE_MIGRATION_ALL = 2 to selection table migration & set 1 to selecting table 

CREATE_MIGRATION_ALL = 1
CREATE_MIGRATION_USERS = 0
CREATE_MIGRATION_BOOKS = 0
```
### Dependencies
- gin v1.5.0
- cors v0.0.0-20170318125340-cf4846e6a636
- gorm v1.23.4
- mysql v1.3.2
- crypto v0.0.0-20210921155107-089bfa567519
- swaggo/gin-swagger v1.2.0
- swaggo/files v0.0.0-20190704085106-630677cd5c14

### How To Run
```
go run main.go
```

### How To Refresh Swagger UI
```
swag init --parseDependency --parseInternal
```

## Results

### http://localhost:8000
![screencapture-localhost-8000-api-docs-index-html-2022-04-16-21_42_50](https://user-images.githubusercontent.com/48195224/163680061-ef00d7ae-7038-4432-81d5-173e220481d5.png)

### http://localhost:8000/swagger/index.html
![screencapture-localhost-8000-api-docs-index-html-2022-04-16-21_42_50](https://i.ibb.co/JQMyvvQ/Screen-Shot-2023-11-08-at-21-23-07.png)


## References
https://www.santekno.com/mengenal-repository-pattern-pada-golang/

https://www.golanglearn.com/golang-tutorials/golang-mvc-project-structure-without-any-framework/#:~:text=The%20Model-View-Controller

https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/13.1.html

https://manakuro.medium.com/clean-architecture-with-go-bce409427d31

https://refactoring.guru/design-patterns/factory-method
