package responses

type Responses struct {
	Error   bool        `json:"error"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ResponsesWithPagination struct {
	Result    interface{} `json:"result"`
	Page      string      `json:"page"`
	Limit     string      `json:"limit"`
	TotalData string      `json:"total_data"`
}

type ResponsesLogin struct {
	DataUser interface{} `json:"data_user"`
	Token    string      `json:"token"`
}
