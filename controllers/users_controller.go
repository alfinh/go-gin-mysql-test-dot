package controllers

import (
	"go-gin-mysql-test-dot/helper"
	"go-gin-mysql-test-dot/lib/config"
	"go-gin-mysql-test-dot/repositories"
	"net/http"

	"github.com/gin-gonic/gin" // package used to read the .env file
	"github.com/rs/zerolog/log"
)

// GetAllUser get all user data
// @Summary get all user data
// @Description This endpoint for login for get all user data.
// @Tags Users (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param page query number false "page for pagination, example (1)" default(1)
// @Param limit query number false "limit for pagination, example (10)" default(10)
// @Produce json
// @Success 200 {object} responses.Responses{data=string}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/users [get]
func GetAllUser(c *gin.Context) {
	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	//handler pagination
	var pagination helper.Pagination
	if err := c.Bind(&pagination); err != nil {
		log.Error().Err(err).Msg("[GetAllUser] failed to Bind")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}
	pagination = pagination.SetDefaultPagination()

	// call repository
	repository := repositories.NewRepositoryUsers(db)
	func(usersRepository repositories.UsersRepository) {
		results, err := repository.FindAll(&pagination)
		if err != nil {
			log.Error().Err(err).Msg("[GetAllUser] failed to call repository FindAll")
			helper.ErrorCustomStatus(res, http.StatusUnprocessableEntity, err.Error())
			return
		}
		helper.Responses(res, http.StatusOK, "Get data users successfully.", results)
	}(repository)

}
