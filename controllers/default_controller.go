package controllers

import (
	"go-gin-mysql-test-dot/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// DefaultRoutes defaults routes
// @Summary defaults routes
// @Description This endpoint for login for defaults routes
// @Tags Health Check
// @Produce json
// @Success 200 {object} responses.Responses{data=string}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/ [get]
func DefaultRoutes(c *gin.Context) {
	var res = c.Writer
	helper.Responses(res, http.StatusOK, "Welcome To Rest API Golang CRUD & Authentication", nil)
}
