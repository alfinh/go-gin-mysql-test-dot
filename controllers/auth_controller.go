package controllers

import (
	"go-gin-mysql-test-dot/helper"
	"go-gin-mysql-test-dot/lib/config"
	"go-gin-mysql-test-dot/repositories"
	"go-gin-mysql-test-dot/validations"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gopkg.in/validator.v2"
)

// Register Register new user
// @Summary Register new user
// @Description This endpoint for register new user.
// @Tags Authentication
// @Param request body validations.RegisterValidation true "The register validation"
// @Produce json
// @Success 201 {object} responses.Responses{data=models.Users}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/register [post]
func Register(c *gin.Context) {
	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	//validation form body for standarize
	var register_validation validations.RegisterValidation
	if err := c.ShouldBindJSON(&register_validation); err != nil {
		log.Error().Err(err).Msg("[Register] failed to ShouldBindJSON")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	if err := validator.Validate(register_validation); err != nil {
		log.Error().Err(err).Msg("[Register] failed to validate request")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	// call repository
	repository := repositories.NewRepositoryAuth(db)
	func(authRepository repositories.AuthRepository) {
		results, err := repository.AuthRegister(&register_validation)
		if err != nil {
			log.Error().Err(err).Msg("[Register] failed to call repository AuthRegister")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}
		helper.Responses(res, http.StatusCreated, "Register successfully.", results)
	}(repository)
}

// Login Login for user
// @Summary Login for user
// @Description This endpoint for login for user.
// @Tags Authentication
// @Param request body validations.LoginValidation true "The login validation"
// @Produce json
// @Success 200 {object} responses.ResponsesLogin{data=string}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/login [post]
func Login(c *gin.Context) {
	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	//validation form body for standarize
	var login_validation validations.LoginValidation
	if err := c.ShouldBindJSON(&login_validation); err != nil {
		log.Error().Err(err).Msg("[Login] failed to ShouldBindJSON")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		c.Abort()
		return
	}

	if err := validator.Validate(login_validation); err != nil {
		log.Error().Err(err).Msg("[Login] failed to validate request")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		c.Abort()
		return
	}

	// call repository
	repository := repositories.NewRepositoryAuth(db)
	func(authRepository repositories.AuthRepository) {
		results, err := repository.AuthLogin(login_validation.UserName, login_validation.Password)
		if err != nil {
			log.Error().Err(err).Msg("[Login] failed to call repository AuthLogin")
			helper.ErrorCustomStatus(res, http.StatusForbidden, "username or password is incorrect.")
			return
		}
		helper.Responses(res, http.StatusOK, "Login successfully.", results)
	}(repository)

}

// Logout Logout for user
// @Summary Logout for user
// @Description This endpoint for login for user.
// @Tags Authentication
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Produce json
// @Success 200 {object} responses.Responses{data=string}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/logout [get]
func Logout(c *gin.Context) {
	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	//check for status
	bearerToken := c.Request.Header.Get("Authorization")

	// call repository
	repository := repositories.NewRepositoryAuth(db)
	func(authRepository repositories.AuthRepository) {
		results, err := repository.AuthLogout(bearerToken)
		if err != nil {
			log.Error().Err(err).Msg("[Logout] failed to call repository AuthLogout")
			helper.ErrorCustomStatus(res, http.StatusUnauthorized, err.Error())
			return
		}
		helper.Responses(res, http.StatusOK, "Logout successfully.", results)
	}(repository)
}

// CurrentUser get current user data logged
// @Summary get current user data logged
// @Description This endpoint for login for get current user data logged.
// @Tags Authentication
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Produce json
// @Success 200 {object} responses.Responses{data=string}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/profile [get]
func CurrentUser(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	// call repository
	repository := repositories.NewRepositoryAuth(db)
	func(authRepository repositories.AuthRepository) {
		results, err := repository.AuthCurrentUser(c)
		if err != nil {
			log.Error().Err(err).Msg("[CurrentUser] failed to call repository AuthCurrentUser")
			helper.ErrorCustomStatus(res, http.StatusUnauthorized, "Unauthorized")
			return
		}
		helper.Responses(res, http.StatusOK, "Get profile successfully.", results)
	}(repository)

}
