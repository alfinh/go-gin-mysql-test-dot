package controllers

import (
	"go-gin-mysql-test-dot/helper"
	"go-gin-mysql-test-dot/lib/config"
	"go-gin-mysql-test-dot/repositories"
	"go-gin-mysql-test-dot/validations"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin" // package used to read the .env file
	"github.com/rs/zerolog/log"
	"gopkg.in/validator.v2"
)

const (
	keyRedis = "book-data"
)

// GetAllBooks Get all books data
// @Summary Get all books data
// @Description This endpoint for get all books data.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Produce json
// @Success 200 {object} responses.Responses{data=[]models.Books}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books [get]
func GetAllBooks(c *gin.Context) {

	// get connection db & redis
	db := config.GetConnection()
	var res = c.Writer

	// check on redis repository
	results, err := getDataToRepositoryRedis(keyRedis)
	if err != nil {
		log.Error().Err(err).Msg("[GetAllBooks] failed getDataToRepositoryRedis")
		helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
		return
	}
	if results != nil {
		helper.Responses(res, http.StatusOK, "Get data books successfully.", results)
		return
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.FindAll()
		if err != nil {
			log.Error().Err(err).Msg("[GetAllBooks] failed to call repository FindAll")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}

		// set result to redis (use go routines)
		go setDataToRepositoryRedis(keyRedis, results)

		helper.Responses(res, http.StatusOK, "Get data books successfully.", results)
	}(repository)

}

func getDataToRepositoryRedis(key string) (data interface{}, err error) {
	// get redis connection
	redis := config.GetRedisConnection()

	// get data to redis repository
	redis_repository := repositories.NewRepositoryRedis(redis)
	func(redisRepository repositories.RedisRepository) {
		data, err = redis_repository.GetDataToRedis(key)
		if err != nil {
			return
		}
	}(redis_repository)

	log.Info().Msg("[getDataToRepositoryRedis] success get data to repository redis")

	return
}

func setDataToRepositoryRedis(key string, data interface{}) (err error) {
	// get redis connection
	redis := config.GetRedisConnection()

	// set data to redis repository
	redis_repository := repositories.NewRepositoryRedis(redis)
	func(redisRepository repositories.RedisRepository) {
		err := redis_repository.SetDataToRedis(key, data)
		if err != nil {
			return
		}
	}(redis_repository)

	log.Info().Msg("[setDataToRepositoryRedis] success set data to repository redis")

	return
}

// GetOneBooks Resolve books by id.
// @Summary Resolve books by id
// @Description This endpoint for resolve books by id.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param id path number false "The books identifier"
// @Produce json
// @Success 200 {object} responses.Responses{data=models.Books}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books/{id} [get]
func GetOneBooks(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	params := c.Params.ByName("id")
	id, err := strconv.ParseInt(params, 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[GetOneBooks] failed to convert id to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.FindById(id)
		if err != nil {
			log.Error().Err(err).Msg("[GetOneBooks] failed to call repository FindById")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}
		helper.Responses(res, http.StatusOK, "Get data books detail successfully.", results)
	}(repository)
}

// CreateBooks Create books data.
// @Summary Create books data
// @Description This endpoint for create books data.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param request body validations.BooksValidation true "The Books to be created."
// @Produce json
// @Success 201 {object} responses.Responses{data=models.Books}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books [post]
func CreateBooks(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	//validation form body for standarize
	var books_validation validations.BooksValidation
	if err := c.ShouldBindJSON(&books_validation); err != nil {
		log.Error().Err(err).Msg("[CreateBooks] failed to shouldBindJSON")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	if err := validator.Validate(books_validation); err != nil {
		log.Error().Err(err).Msg("[CreateBooks] failed to validate request")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	//created_by
	var params string = c.MustGet("user_id").(string)
	s := strings.Split(params, ".")
	created_by, err := strconv.ParseInt(s[0], 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[CreateBooks] failed to convert string to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.Save(&books_validation, created_by)
		if err != nil {
			log.Error().Err(err).Msg("[CreateBooks] failed to call repository Save")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}

		// set result to redis (use go routines)
		go setDataToRepositoryRedis(keyRedis, nil)

		helper.Responses(res, http.StatusCreated, "Create data books detail successfully.", results)
	}(repository)
}

// CreateBulkBooks Create bulk books data.
// @Summary Create bulk books data
// @Description This endpoint for create bulk books data.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param request body validations.BulkBooksValidation true "The Bulk Books data to be created."
// @Produce json
// @Success 201 {object} responses.Responses{data=[]models.Books}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books/bulk [post]
func CreateBulkBooks(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	//validation form body for standarize
	var bulk_books_validation validations.BulkBooksValidation
	if err := c.ShouldBindJSON(&bulk_books_validation); err != nil {
		log.Error().Err(err).Msg("[CreateBulkBooks] failed to shouldBindJSON")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	if len(bulk_books_validation) == 0 {
		helper.ErrorCustomStatus(res, http.StatusBadRequest, "body of the request being processed is empty")
		return
	}

	if len(bulk_books_validation) > 20 {
		helper.ErrorCustomStatus(res, http.StatusBadRequest, "maximum number of bulk of create is 20 data")
		return
	}

	for _, books_validation := range bulk_books_validation {
		if err := validator.Validate(books_validation); err != nil {
			log.Error().Err(err).Msg("[CreateBulkBooks] failed to validate request")
			helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
			return
		}
	}

	//created_by
	var params string = c.MustGet("user_id").(string)
	s := strings.Split(params, ".")
	created_by, err := strconv.ParseInt(s[0], 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[CreateBulkBooks] failed to convert string to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.SaveWithTx(&bulk_books_validation, created_by)
		if err != nil {
			log.Error().Err(err).Msg("[CreateBulkBooks] failed to call repository SaveWithTx")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}

		// set result to redis (use go routines)
		go setDataToRepositoryRedis(keyRedis, nil)

		helper.Responses(res, http.StatusCreated, "Bulk create data books detail successfully.", results)
	}(repository)
}

// UpdateBooks Update books data.
// @Summary Update books data
// @Description This endpoint for update books data.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param id path number false "The books identifier"
// @Param request body validations.BooksValidation true "The Books data to be updated."
// @Produce json
// @Success 200 {object} responses.Responses{data=models.Books}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books/{id} [put]
func UpdateBooks(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	id := c.Params.ByName("id")
	var res = c.Writer

	//validation form body for standarize
	var books_validation validations.BooksValidation
	if err := c.ShouldBindJSON(&books_validation); err != nil {
		log.Error().Err(err).Msg("[UpdateBooks] failed to shouldBindJSON")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	if err := validator.Validate(books_validation); err != nil {
		log.Error().Err(err).Msg("[UpdateBooks] failed to validate request")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	//updated_by
	var params string = c.MustGet("user_id").(string)
	s := strings.Split(params, ".")
	updated_by, err := strconv.ParseInt(s[0], 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[UpdateBooks] failed to convert string to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	//id_books
	id_books, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.Update(id_books, &books_validation, updated_by)
		if err != nil {
			log.Error().Err(err).Msg("[UpdateBooks] failed to call repository Update")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}

		// set result to redis (use go routines)
		go setDataToRepositoryRedis(keyRedis, nil)

		helper.Responses(res, http.StatusOK, "Update data books successfully.", results)
	}(repository)

}

// UpdatePublisherBooks Update publisher books data.
// @Summary Update publisher books data
// @Description This endpoint for update publisher books data.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param id path number false "The books identifier"
// @Param request body validations.BooksPublisherValidation true "The Publisher Books to be updated."
// @Produce json
// @Success 200 {object} responses.Responses{data=models.Books}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books/{id}/publish [patch]
func UpdatePublisherBooks(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	id := c.Params.ByName("id")
	var res = c.Writer

	//validation form body for standarize
	var books_validation validations.BooksPublisherValidation
	if err := c.ShouldBindJSON(&books_validation); err != nil {
		log.Error().Err(err).Msg("[UpdatePublisherBooks] failed to shouldBindJSON")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	if err := validator.Validate(books_validation); err != nil {
		log.Error().Err(err).Msg("[UpdatePublisherBooks] failed to validate request")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	//updated_by
	var params string = c.MustGet("user_id").(string)
	s := strings.Split(params, ".")
	updated_by, err := strconv.ParseInt(s[0], 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[UpdatePublisherBooks] failed to convert string to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	//id_books
	id_books, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[UpdatePublisherBooks] failed to convert string to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.UpdatePublisher(id_books, &books_validation, updated_by)
		if err != nil {
			log.Error().Err(err).Msg("[UpdateBooks] failed to call repository UpdatePublisher")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}

		// set result to redis (use go routines)
		go setDataToRepositoryRedis(keyRedis, nil)

		helper.Responses(res, http.StatusOK, "Update data books successfully.", results)
	}(repository)

}

// DeleteBooks Delete books data.
// @Summary Delete books data
// @Description This endpoint for delete books data.
// @Tags Books (Authorized)
// @Param Authorization header string true "Bearer xxxxxxxxx"
// @Param id path number false "The books identifier"
// @Produce json
// @Success 200 {object} responses.Responses{data=string}
// @Failure 400 {object} responses.Responses{data=string}
// @Failure 404 {object} responses.Responses{data=string}
// @Failure 500 {object} responses.Responses{data=string}
// @Router /api/v1/books/{id} [delete]
func DeleteBooks(c *gin.Context) {

	// get connection db
	db := config.GetConnection()
	var res = c.Writer

	params := c.Params.ByName("id")
	id, err := strconv.ParseInt(params, 10, 64)
	if err != nil {
		log.Error().Err(err).Msg("[DeleteBooks] failed to convert string to int")
		helper.ErrorCustomStatus(res, http.StatusBadRequest, err.Error())
		return
	}

	// call repository
	repository := repositories.NewRepositoryBooks(db)
	func(booksRepository repositories.BooksRepository) {
		results, err := repository.Delete(id)
		if err != nil {
			log.Error().Err(err).Msg("[DeleteBooks] failed to call repository Delete")
			helper.ErrorCustomStatus(res, http.StatusInternalServerError, err.Error())
			return
		}

		// set result to redis (use go routines)
		go setDataToRepositoryRedis(keyRedis, nil)

		helper.Responses(res, http.StatusOK, "Delete data books successfully.", results)
	}(repository)
}
