package repositories

import (
	"go-gin-mysql-test-dot/helper"
	"go-gin-mysql-test-dot/models"
	"go-gin-mysql-test-dot/responses"
	validations "go-gin-mysql-test-dot/validations"
	"strconv"

	"gorm.io/gorm"
)

//interface
type BooksRepository interface {
	Save(books_validation *validations.BooksValidation, created_by int64) (interface{}, error)
	SaveWithTx(bulk_books_validation *validations.BulkBooksValidation, created_by int64) ([]interface{}, error)
	FindAll() (interface{}, error)
	FindAllWithPagination(pagination *helper.Pagination) (interface{}, error)
	FindById(id int64) (interface{}, error)
	Update(id int64, books_validation *validations.BooksValidation, updated_by int64) (interface{}, error)
	Delete(id int64) (interface{}, error)
	UpdatePublisher(id int64, books_validation *validations.BooksPublisherValidation, updated_by int64) (interface{}, error)
}

type respositoryBooks struct {
	db *gorm.DB
}

func NewRepositoryBooks(db *gorm.DB) *respositoryBooks {
	return &respositoryBooks{db}
}

func (r *respositoryBooks) FindAllWithPagination(pagination *helper.Pagination) (interface{}, error) {
	var responses responses.ResponsesWithPagination
	var books []models.Books

	// query data
	result := r.db.Scopes(helper.Paginate(pagination.Limit, pagination.Page)).Order("created_at desc").Find(&books)
	if result.Error != nil {
		return nil, result.Error
	}

	// count data
	var books_count []models.Books
	var data_count int64
	r.db.Model(&books_count).Count(&data_count)

	//response
	responses.Result = books
	responses.Limit = pagination.Limit
	responses.Page = pagination.Page
	responses.TotalData = strconv.FormatInt(data_count, 10)
	return responses, nil
}

func (r *respositoryBooks) FindAll() (interface{}, error) {
	var responses responses.Responses
	var books []models.Books

	// query data
	result := r.db.Order("created_at desc").Find(&books)
	if result.Error != nil {
		return nil, result.Error
	}

	//response
	responses.Data = books
	return responses, nil
}

func (r *respositoryBooks) Save(books_validation *validations.BooksValidation, created_by int64) (interface{}, error) {

	//add data books to database
	var books models.Books
	books.BookNumber = books_validation.BookNumber
	books.BookTitle = books_validation.BookTitle
	books.Author = books_validation.Author
	books.PublicationYear = books_validation.PublicationYear
	books.Publisher = books_validation.Publisher
	books.CreatedBy = created_by

	//save db
	r.db.Create(&books)
	return books, nil
}

func (r *respositoryBooks) SaveWithTx(bulk_books_validation *validations.BulkBooksValidation, created_by int64) ([]interface{}, error) {
	tx := r.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	var ListBooks []interface{}
	for _, book := range *bulk_books_validation {
		result, err := r.Save(&book, created_by)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
		ListBooks = append(ListBooks, result)
	}

	return ListBooks, tx.Commit().Error
}

func (r *respositoryBooks) FindById(id int64) (interface{}, error) {
	var books models.Books
	if err := r.db.Where("id = ?", id).First(&books).Error; err != nil {
		return nil, err
	} else {
		return books, nil
	}
}

func (r *respositoryBooks) Update(id int64, books_validation *validations.BooksValidation, updated_by int64) (interface{}, error) {

	// check by id
	var books models.Books
	if err := r.db.Where("id = ?", id).First(&books).Error; err != nil {
		return nil, err
	} else {
		//update data books to database
		books.BookNumber = books_validation.BookNumber
		books.BookTitle = books_validation.BookTitle
		books.Author = books_validation.Author
		books.PublicationYear = books_validation.PublicationYear
		books.Publisher = books_validation.Publisher
		books.UpdatedBy = updated_by

		//save db
		r.db.Save(&books)
		return books, nil
	}
}

func (r *respositoryBooks) UpdatePublisher(id int64, books_validation *validations.BooksPublisherValidation, updated_by int64) (interface{}, error) {

	// check by id
	var books models.Books
	if err := r.db.Where("id = ?", id).First(&books).Error; err != nil {
		return nil, err
	} else {
		//update data books to database
		books.PublicationYear = books_validation.PublicationYear
		books.Publisher = books_validation.Publisher
		books.UpdatedBy = updated_by

		//save db
		r.db.Save(&books)
		return books, nil
	}
}

func (r *respositoryBooks) Delete(id int64) (interface{}, error) {
	var books models.Books
	if err := r.db.Where("id = ?", id).First(&books).Error; err != nil {
		return nil, err
	} else {

		//delete data
		_ = r.db.Where("id = ?", id).Delete(&books)
		return nil, nil
	}
}
