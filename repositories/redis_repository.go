package repositories

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

const (
	prefix = "local"
)

//interface
type RedisRepository interface {
	SetDataToRedis(key string, data interface{}) (err error)
	GetDataToRedis(key string) (data interface{}, err error)
}

type respositoryRedis struct {
	Client *redis.Client
}

func NewRepositoryRedis(redis *redis.Client) *respositoryRedis {
	return &respositoryRedis{redis}
}

func (repo *respositoryRedis) SetDataToRedis(key string, data interface{}) (err error) {
	key = fmt.Sprintf("%s:%s", prefix, key)
	expiration := 1 * time.Hour // TODO: redis ttl static 1 hour, maybe next development set on config (environtment variable)

	// Serialize the array of structs to a byte slice
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return
	}

	_, err = repo.Client.Set(key, jsonBytes, expiration).Result()
	if err != nil {
		return
	}

	return
}

func (repo *respositoryRedis) GetDataToRedis(key string) (data interface{}, err error) {
	key = fmt.Sprintf("%s:%s", prefix, key)
	ruleDataStr, err := repo.Client.Get(key).Result()
	if err != nil {
		if err == redis.Nil {
			err = nil
			return
		}
		return
	}
	err = json.Unmarshal([]byte(ruleDataStr), &data)
	if err != nil {
		return
	}

	return
}
