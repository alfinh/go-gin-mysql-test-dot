definitions:
  gorm.DeletedAt:
    properties:
      time:
        type: string
      valid:
        description: Valid is true if Time is not NULL
        type: boolean
    type: object
  models.Books:
    properties:
      author:
        type: string
      book_number:
        type: string
      book_title:
        type: string
      created_at:
        description: timestamps
        type: string
      created_by:
        type: integer
      deleted_at:
        allOf:
        - $ref: '#/definitions/gorm.DeletedAt'
        description: timestamps
      id:
        description: big integer
        type: integer
      publication_year:
        type: integer
      publisher:
        type: string
      updated_at:
        description: timestamps
        type: string
      updated_by:
        type: integer
    type: object
  models.Users:
    properties:
      created_at:
        description: timestamps
        type: string
      created_by:
        type: integer
      deleted_at:
        allOf:
        - $ref: '#/definitions/gorm.DeletedAt'
        description: timestamps
      id:
        description: big integer
        type: integer
      name:
        type: string
      password:
        type: string
      token:
        type: string
      updated_at:
        description: timestamps
        type: string
      updated_by:
        type: integer
      user_name:
        type: string
    type: object
  responses.Responses:
    properties:
      code:
        type: integer
      data: {}
      error:
        type: boolean
      message:
        type: string
    type: object
  responses.ResponsesLogin:
    properties:
      data_user: {}
      token:
        type: string
    type: object
  validations.BooksPublisherValidation:
    properties:
      publication_year:
        minimum: 1
        type: integer
      publisher:
        minLength: 1
        type: string
    required:
    - publication_year
    - publisher
    type: object
  validations.BooksValidation:
    properties:
      author:
        minLength: 1
        type: string
      book_number:
        minLength: 1
        type: string
      book_title:
        minLength: 1
        type: string
      publication_year:
        minimum: 1
        type: integer
      publisher:
        minLength: 1
        type: string
    required:
    - author
    - book_number
    - book_title
    - publication_year
    - publisher
    type: object
  validations.LoginValidation:
    properties:
      password:
        maxLength: 225
        minLength: 6
        type: string
      user_name:
        maxLength: 45
        minLength: 6
        type: string
    required:
    - password
    - user_name
    type: object
  validations.RegisterValidation:
    properties:
      name:
        maxLength: 45
        minLength: 6
        type: string
      password:
        maxLength: 225
        minLength: 6
        type: string
      user_name:
        maxLength: 225
        minLength: 6
        type: string
    required:
    - name
    - password
    - user_name
    type: object
info:
  contact: {}
paths:
  /api/v1/:
    get:
      description: This endpoint for login for defaults routes
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: defaults routes
      tags:
      - Health Check
  /api/v1/books:
    get:
      description: This endpoint for get all books data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  items:
                    $ref: '#/definitions/models.Books'
                  type: array
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Get all books data
      tags:
      - Books (Authorized)
    post:
      description: This endpoint for create books data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - description: The Books to be created.
        in: body
        name: request
        required: true
        schema:
          $ref: '#/definitions/validations.BooksValidation'
      produces:
      - application/json
      responses:
        "201":
          description: Created
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  $ref: '#/definitions/models.Books'
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Create books data
      tags:
      - Books (Authorized)
  /api/v1/books/{id}:
    delete:
      description: This endpoint for delete books data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - description: The books identifier
        in: path
        name: id
        type: number
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Delete books data
      tags:
      - Books (Authorized)
    get:
      description: This endpoint for resolve books by id.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - description: The books identifier
        in: path
        name: id
        type: number
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  $ref: '#/definitions/models.Books'
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Resolve books by id
      tags:
      - Books (Authorized)
    put:
      description: This endpoint for update books data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - description: The books identifier
        in: path
        name: id
        type: number
      - description: The Books data to be updated.
        in: body
        name: request
        required: true
        schema:
          $ref: '#/definitions/validations.BooksValidation'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  $ref: '#/definitions/models.Books'
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Update books data
      tags:
      - Books (Authorized)
  /api/v1/books/{id}/publish:
    patch:
      description: This endpoint for update publisher books data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - description: The books identifier
        in: path
        name: id
        type: number
      - description: The Publisher Books to be updated.
        in: body
        name: request
        required: true
        schema:
          $ref: '#/definitions/validations.BooksPublisherValidation'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  $ref: '#/definitions/models.Books'
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Update publisher books data
      tags:
      - Books (Authorized)
  /api/v1/books/bulk:
    post:
      description: This endpoint for create bulk books data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - description: The Bulk Books data to be created.
        in: body
        name: request
        required: true
        schema:
          items:
            $ref: '#/definitions/validations.BooksValidation'
          type: array
      produces:
      - application/json
      responses:
        "201":
          description: Created
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  items:
                    $ref: '#/definitions/models.Books'
                  type: array
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Create bulk books data
      tags:
      - Books (Authorized)
  /api/v1/login:
    post:
      description: This endpoint for login for user.
      parameters:
      - description: The login validation
        in: body
        name: request
        required: true
        schema:
          $ref: '#/definitions/validations.LoginValidation'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.ResponsesLogin'
            - properties:
                data:
                  type: string
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Login for user
      tags:
      - Authentication
  /api/v1/logout:
    get:
      description: This endpoint for login for user.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Logout for user
      tags:
      - Authentication
  /api/v1/profile:
    get:
      description: This endpoint for login for get current user data logged.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: get current user data logged
      tags:
      - Authentication
  /api/v1/register:
    post:
      description: This endpoint for register new user.
      parameters:
      - description: The register validation
        in: body
        name: request
        required: true
        schema:
          $ref: '#/definitions/validations.RegisterValidation'
      produces:
      - application/json
      responses:
        "201":
          description: Created
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  $ref: '#/definitions/models.Users'
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: Register new user
      tags:
      - Authentication
  /api/v1/users:
    get:
      description: This endpoint for login for get all user data.
      parameters:
      - description: Bearer xxxxxxxxx
        in: header
        name: Authorization
        required: true
        type: string
      - default: 1
        description: page for pagination, example (1)
        in: query
        name: page
        type: number
      - default: 10
        description: limit for pagination, example (10)
        in: query
        name: limit
        type: number
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "400":
          description: Bad Request
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "404":
          description: Not Found
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
        "500":
          description: Internal Server Error
          schema:
            allOf:
            - $ref: '#/definitions/responses.Responses'
            - properties:
                data:
                  type: string
              type: object
      summary: get all user data
      tags:
      - Users (Authorized)
swagger: "2.0"
