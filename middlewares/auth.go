package middlewares

import (
	"net/http"

	token "go-gin-mysql-test-dot/utils"

	"github.com/gin-gonic/gin"
)

func JwtAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code":    401,
				"message": "Unathorized",
				"data":    nil,
			})
			c.Abort()
			return
		}
		c.Next()
	}
}

func CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		user_id, err := token.ExtractTokenID(c)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code":    401,
				"message": "Unathorized",
				"data":    nil,
			})
			c.Abort()
			return
		} else {
			c.Set("user_id", user_id) // data for controller
			c.Next()
		}
	}
}
